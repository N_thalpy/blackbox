﻿using System;

namespace Blackbox.GameLoop
{
    public abstract class GameLoopBase : IDisposable
    {
        public readonly String DebugName;
        private bool isInitialized;

        public GameLoopBase(String debugName)
        {
            DebugName = debugName;
            isInitialized = false;
        }
        
        public void ResetInitializeState()
        {
            isInitialized = false;
        }
        public void Initialize()
        {
            if (isInitialized == false)
               OnInitialize();

            isInitialized = true;
        }

        protected abstract void OnInitialize();
        public abstract void Render(Second deltaTime);
        public abstract void Update(Second deltaTime);

        public virtual void Dispose()
        {
        }
    }
}
