﻿using Blackbox.Sound;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
using System;

namespace Blackbox.GameLoop.Test
{
    public sealed class ALTestLoop : GameLoopBase
    {
        int buffer;
        int source;

        AudioContext ac;

        public ALTestLoop()
            : base("ALTestLoop")
        {

        }

        protected override void OnInitialize()
        {
            ac = new AudioContext();

            AL.Listener(ALListenerf.Gain, 10);

            buffer = AL.GenBuffer();
            source = AL.GenSource();

            int sampleRate = 44100;
            int hz = 220;
            int length = 10;
            Int16[] sample = new Int16[sampleRate * length];

            for (int i = 0; i < sample.Length; i++)
                sample[i] = (Int16)(Int16.MaxValue * Math.Sin(2 * Math.PI / sampleRate * i * hz));

            AL.BufferData(buffer, ALFormat.Mono16, sample, sample.Length, sampleRate);
            ALHelper.CheckALError();

            AL.BindBufferToSource(source, buffer);
            AL.Source(source, ALSourcef.Gain, 0.1f);
            AL.Source(source, ALSourcef.Pitch, 2);
               
            AL.SourcePlay(source);
            ALHelper.CheckALError();
        }

        public override void Update(Second deltaTime)
        {
        }
        public override void Render(Second deltaTime)
        {
        }
    }
}
