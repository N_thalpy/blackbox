﻿using Blackbox.BlackboxSystem;
using Blackbox.GameObject.Logic.Automata;
using Blackbox.GameObject.Logic.Data;

namespace Blackbox.GameLoop.Test
{
    public sealed class TestGameLoop : GameLoopBase
    {
        DFA dfa;

        public TestGameLoop()
            : base("TessGameLoop")
        {
        }

        protected override void OnInitialize()
        {
            dfa = DataFactory.Tutorial2;
            dfa.Position = GameSystem.MainForm.Viewport / 2;
        }
        public override void Update(Second deltaTime)
        {
            dfa.Update();
        }
        public override void Render(Second deltaTime)
        {
            dfa.Render();
        }
    }
}
