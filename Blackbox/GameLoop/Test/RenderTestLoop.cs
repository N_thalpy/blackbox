﻿using Blackbox.BlackboxSystem;
using Blackbox.GameObject.Renderable;
using Blackbox.Input;
using Blackbox.Rendering.BitmapFont;
using OpenTK;
using System;
using System.Drawing;

namespace Blackbox.GameLoop.Test
{
    public sealed class RenderTestLoop : GameLoopBase
    {
        Circle[] c;
        Arrow[] a;
        Label[] l;

        public RenderTestLoop()
            : base("RenderTest")
        {
            c = new Circle[2];
            a = new Arrow[9];
            l = new Label[1];
        }

        protected override void OnInitialize()
        {
            c[0] = new Circle(100, 10, Color.White, Color.Green);
            c[0].Position = GameSystem.MainForm.Viewport / 2;

            c[1] = new Circle(300, 10, Color.White, Color.Blue);
            c[1].Position = GameSystem.MainForm.Viewport / 2;

            for (int i = 0; i < 8; i++)
            {
                Radian angle = Radian.PI * 2 * (i + 1) / 9 - Radian.PI / 4;

                a[i] = new Arrow(32, 64, Color.Red);
                a[i].Angle = angle;
                a[i].Length = 200;
                a[i].Anchor = -100;
                a[i].Position = GameSystem.MainForm.Viewport / 2;
            }

            a[8] = new Arrow(32, 64, Color.Cyan);
            a[8].Angle = Radian.PI / 4;
            a[8].Length = 450;
            a[8].Anchor = 0;
            a[8].Position = GameSystem.MainForm.Viewport / 2;

            l[0] = new Label(BitmapFont.Create("Resource/font.fnt"));
            l[0].Text = "123123 abcdefgh";
            l[0].Anchor = new Vector2(0, 0);
            l[0].Position = Vector2.Zero;
        }

        public override void Update(Second deltaTime)
        {
            if (InputHelper.GetKey() == InputType.Dot)
                a[8].Angle += Radian.PI / 8;
        }
        public override void Render(Second deltaTime)
        {
            c[1].Render();
            c[0].Render();
            foreach (var e in a)
                e.Render();

            l[0].Render();
        }
    }
}
