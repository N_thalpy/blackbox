﻿using Blackbox.BlackboxSystem;
using Blackbox.GameObject.Logic;
using Blackbox.GameObject.Logic.Automata;
using Blackbox.GameObject.Renderable;
using Blackbox.Input;
using Blackbox.Rendering.BitmapFont;
using OpenTK;
using System;

namespace Blackbox.GameLoop.Impl
{
    public sealed class ProblemLoop : GameLoopBase
    {
        Label problemStateLabel;
        Label acceptStateLabel;
        bool accepted;

        AutomataBase problem;

        public ProblemLoop(AutomataBase a)
            : base("ProblemLoop")
        {
            problem = a;
            problem.Position = GameSystem.MainForm.Viewport / 2;
            accepted = false;
        } 

        protected override void OnInitialize()
        {
            accepted = false;
            problem.Reset();

            problemStateLabel = new Label(BitmapFontProvider.Font);
            problemStateLabel.Anchor = new Vector2(0, 0);
            problemStateLabel.Text = String.Empty;
            problemStateLabel.Position = Vector2.Zero;

            acceptStateLabel = new Label(BitmapFontProvider.Font);
            acceptStateLabel.Anchor = new Vector2(1, 0);
            acceptStateLabel.Text = String.Empty;
            acceptStateLabel.Position = new Vector2(GameSystem.MainForm.Viewport.X, 0);
        }
        public override void Update(Second deltaTime)
        {
            if (accepted == true && InputHelper.GetKey() != InputType.None)
                GameSystem.PopGameLoop();

            if (InputHelper.GetKey() == InputType.LongDash)
                if (problem.IsDirty() == false)
                {
                    GameSystem.PopGameLoop();
                    return;
                }

            problem.Update();
            if (problem.Accepted() == true)
            {
                UserData.SetState(problem, UserData.ProblemState.Solved);
                accepted = true;
            }
        }
        public override void Render(Second deltaTime)
        {
            problemStateLabel.Text = String.Format("{0}\n{1}", problem.Constraint.GetConstraintString(), problem.Constraint.GetCurrentStateString());

            if (problem.Accepted() == true)
                acceptStateLabel.Text = "SOLVED!";
            else
                acceptStateLabel.Text = UserData.GetState(problem) == UserData.ProblemState.Solved ? "ALREADY SOLVED" : "NOT SOLVED YET";

            problemStateLabel.Render();
            acceptStateLabel.Render();
            problem.Render();
        }
    }
}
