﻿using Blackbox.BlackboxSystem;
using Blackbox.GameObject.Logic.Automata;
using Blackbox.GameObject.Logic.Data;
using Blackbox.GameObject.Renderable;
using Blackbox.Input;
using Blackbox.Rendering.BitmapFont;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Text;
using static Blackbox.GameLoop.Impl.SelectLoop;

namespace Blackbox.GameLoop.Impl
{
    public sealed class BootLoop : GameLoopBase
    {
        Label progression;
        StringBuilder sb;

        Second timer;
        List<InputType> pw;
        bool authorized;
        
        public BootLoop()
            : base("BootLoop")
        {
            sb = new StringBuilder();
            timer = Second.Zero;

            pw = new List<InputType>();
            authorized = false;
        }

        protected override void OnInitialize()
        {
            progression = new Label(BitmapFontProvider.Font);
            progression.Text = String.Empty;
            progression.FontScale = 2;
            progression.Position = new Vector2(0, GameSystem.MainForm.Viewport.Y);
            progression.Anchor = new Vector2(0, 1);

            AppendString("Blackbox Machine Booted\n");
            AppendString("Blackbox R{0}\n", Config.Revision);
            AppendString("Insert Password:");
        }
        public override void Update(Second deltaTime)
        {
            timer += deltaTime;
            progression.Text = sb.ToString();

            if (authorized == false)
            {
                InputType k = InputHelper.GetKey();
                switch (k)
                {
                    case InputType.Dot:
                    case InputType.Dash:
                        pw.Add(k);
                        break;

                    case InputType.LongDash:
                        pw.Clear();
                        break;
                }

                progression.Text += new string('*', pw.Count);
                if (pw.Count == Config.PasswordLength)
                    if (Config.CheckPassword(pw))
                    {
                        AppendString("{0}\n", new String('*', pw.Count));
                        AppendString("Authorized!\n");

                        authorized = true;
                        timer = Second.Zero;
                    }

                // Append Caret
                if (timer % 2 < 1)
                    progression.Text += "|";
            }
            else
            {
                UserData.Initialize();
                if (timer > (Second)1)
                {
                    SelectLoop sect0 = new SelectLoop(new Component[]
                    {
                        Component.CreateFromAutomata(DataFactory.Tutorial0, null),
                        Component.CreateFromAutomata(DataFactory.Tutorial1, new AutomataBase[] { DataFactory.Tutorial0 }),
                        Component.CreateFromAutomata(DataFactory.Tutorial2, new AutomataBase[] { DataFactory.Tutorial1 }),
                        Component.CreateFromAutomata(DataFactory.NFA0, new AutomataBase[] { DataFactory.Tutorial2 }),
                    });

                    GameSystem.PopGameLoop();
                    GameSystem.PushGameLoop(
                        new SelectLoop(new Component[]
                        {
                            new Component() { Scene = sect0, Text = "Sect 0", Accesable = () => true },
                            new Component() { Scene = null, Text = "Sect 1", Accesable = () => false },
                            new Component() { Scene = null, Text = "Sect 2", Accesable = () => false },
                            new Component() { Scene = null, Text = "Sect 3", Accesable = () => false },
                            new Component() { Scene = null, Text = "Sect 4", Accesable = () => false },
                            new Component() { Scene = null, Text = "Sect 5", Accesable = () => false },
                        }));
                }
            }

        }
        public override void Render(Second deltaTime)
        {
            progression.Render();
        }

        public void AppendString(String s, params Object[] args)
        {
            sb.Append(String.Format(s, args));
        }
    }
}
