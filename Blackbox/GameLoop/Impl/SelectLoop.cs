﻿using Blackbox.BlackboxSystem;
using Blackbox.GameObject.Logic.Automata;
using Blackbox.GameObject.Renderable;
using Blackbox.Input;
using Blackbox.Rendering.BitmapFont;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Blackbox.GameLoop.Impl
{
    public sealed class SelectLoop : GameLoopBase
    {
        public sealed class Component
        {
            public GameLoopBase Scene;
            public String Text;
            public Func<bool> Accesable;

            public static Component CreateFromAutomata(AutomataBase problem, IEnumerable<AutomataBase> precedent)
            {
                return new Component()
                {
                    Scene = new ProblemLoop(problem),
                    Text = problem.Name,
                    Accesable = () =>
                    {
                        if (precedent == null)
                            return true;

                        foreach (AutomataBase a in precedent)
                            if (UserData.GetState(a) == UserData.ProblemState.NotSolved)
                                return false;

                        return true;
                    }
                };
            }
        }

        Circle c;

        int componentCount;
        Component[] components;

        Arrow selector;
        Arrow[] invalidArrow;
        Arrow[] validArrow;
        Label[] l;
        
        int selected;

        public SelectLoop(Component[] components)
            : base("SelectLoop")
        {
            this.componentCount = components.Length;
            this.components = components;
            selected = 0;

            validArrow = new Arrow[componentCount];
            invalidArrow = new Arrow[componentCount];
            l = new Label[componentCount];
        }

        protected override void OnInitialize()
        {
            c = new Circle(150, 10, Color.White, Color.Blue);
            c.Position = GameSystem.MainForm.Viewport / 2;

            for (int idx = 0; idx < componentCount; idx++)
            {
                validArrow[idx] = new Arrow(32, 64, Color.White);
                validArrow[idx].Length = 250;
                validArrow[idx].Position = GameSystem.MainForm.Viewport / 2;
                validArrow[idx].Anchor = 0;
                validArrow[idx].Angle = 2 * Radian.PI * idx / componentCount;

                invalidArrow[idx] = new Arrow(32, 64, Color.Red);
                invalidArrow[idx].Length = 250;
                invalidArrow[idx].Position = GameSystem.MainForm.Viewport / 2;
                invalidArrow[idx].Anchor = 0;
                invalidArrow[idx].Angle = 2 * Radian.PI * idx / componentCount;
            }
            for (int idx = 0; idx <  componentCount; idx++)
            {
                l[idx] = new Label(BitmapFontProvider.Font);
                l[idx].Text = components[idx].Text;
                l[idx].Position = invalidArrow[idx].Position + 300 * invalidArrow[idx].Angle.ToUnitVector();
                l[idx].Anchor = new Vector2(1 - Math.Sign(invalidArrow[idx].Angle.ToUnitVector().X), 1) / 2;
            }

            selector = new Arrow(32, 64, Color.Blue);
            selector.Length = 0;
            selector.Position = GameSystem.MainForm.Viewport / 2;
            selector.Anchor = -250;
        }

        public override void Update(Second deltaTime)
        {
            InputType key = InputHelper.GetKey();
            switch (key)
            {
                case InputType.Dot:
                    selected = (selected + 1) % componentCount;
                    break;

                case InputType.Dash:
                    if (components[selected].Accesable() == true)
                        GameSystem.PushGameLoop(components[selected].Scene);
                    break;

                case InputType.LongDash:
                    GameSystem.PopGameLoop();
                    break;
            }
        }
        public override void Render(Second deltaTime)
        {
            c.Render();
            for (int idx = 0; idx < componentCount; idx++)
                if (components[idx].Accesable())
                    validArrow[idx].Render();
                else
                    invalidArrow[idx].Render();

            selector.Angle = validArrow[selected].Angle;
            selector.Render();

            foreach (Label e in l)
                e.Render();
        }
    }
}
