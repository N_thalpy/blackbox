﻿using System;

namespace Blackbox.GameLoop.Impl
{
    // TODO: Add some effects, such as 
    //
    // > Disconnecting with Server...
    // > Disposing Objects...
    public sealed class ExitLoop : GameLoopBase
    {
        public ExitLoop()
            : base ("ExitLoop")
        {
        }

        protected override void OnInitialize()
        {
        }
        public override void Update(Second deltaTime)
        {
            Environment.Exit(0);
        }
        public override void Render(Second deltaTime)
        {
        }
    }
}
