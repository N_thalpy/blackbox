﻿using System;
using System.Diagnostics;
using Blackbox.BlackboxSystem;
using Blackbox.Rendering.Shader;
using Blackbox.Rendering.VertexType;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Blackbox.Rendering
{
    public static class GLHelper
    {
        private static VertexScreenPositionColorTexture[] vertices = new VertexScreenPositionColorTexture[4];
        private static VertexBuffer vbo;
        private static ElementBuffer ebo;

        [Conditional("DEBUG")]
        public static void CheckGLError()
        {
            var error = GL.GetError();
            Debug.Assert(error == ErrorCode.NoError);

            if (error != ErrorCode.NoError)
                throw new Exception(error.ToString());
        }

        public static void DrawTexture(RenderParameter param)
        {
            if (vbo == null && ebo == null)
            {
                vbo = VertexBuffer.CreateDynamic(vertices, ShaderProvider.ScreenSpaceTexture);
                int[] indices = new int[6];
                indices[0] = 0;
                indices[1] = 1;
                indices[2] = 2;
                indices[3] = 2;
                indices[4] = 1;
                indices[5] = 3;
                ebo = ElementBuffer.Create(indices);
            }

            Texture texture = param.Texture;
            Vector2 size = param.Size;
            Vector2 anchor = param.Anchor;
            Radian angle = param.Angle;
            ShaderBase shaderType = param.ShaderType;
            Vector2 minUV = param.TextureUVMin;
            Vector2 maxUV = param.TextureUVMax;
            Matrix3 mat = Matrix3.Identity;

            Vector2 pos = param.Position;
            Color4 color = Color4.White;

            float left = -anchor.X * size.X;
            float right = (1 - anchor.X) * size.X;
            float up = -anchor.Y * size.Y;
            float down = (1 - anchor.Y) * size.Y;

            vertices[0].TextureUV = new Vector2(minUV.X, minUV.Y);
            vertices[0].Position = pos + new Vector2(left, up).Rotate(angle);
            vertices[0].Color = color;

            vertices[1].TextureUV = new Vector2(maxUV.X, minUV.Y);
            vertices[1].Position = pos + new Vector2(right, up).Rotate(angle);
            vertices[1].Color = color;

            vertices[2].TextureUV = new Vector2(minUV.X, maxUV.Y);
            vertices[2].Position = pos + new Vector2(left, down).Rotate(angle);
            vertices[2].Color = color;

            vertices[3].TextureUV = new Vector2(maxUV.X, maxUV.Y);
            vertices[3].Position = pos + new Vector2(right, down).Rotate(angle);
            vertices[3].Color = color;

            shaderType.Bind(ref mat, new Vector2(GameSystem.MainForm.Width, GameSystem.MainForm.Height), texture);
            vbo.Bind(vertices);
            ebo.Bind();
            GLHelper.CheckGLError();

            GL.DrawElements(PrimitiveType.Triangles, ebo.Count, DrawElementsType.UnsignedInt, 0);
            GLHelper.CheckGLError();
        }
    }
}
