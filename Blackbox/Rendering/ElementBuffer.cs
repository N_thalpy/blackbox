﻿using System;
using System.Diagnostics;
using OpenTK.Graphics.OpenGL;

namespace Blackbox.Rendering
{
    public class ElementBuffer : IDisposable
    {
        private int ebo;
        public int Count { get; private set;}

        #region .ctor
        private ElementBuffer(int ebo, int count)
        {
            this.ebo = ebo;
            this.Count = count;
        }
        public static ElementBuffer Create(int[] elements)
        {
            int count = elements.Length;
            int ebo = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ebo);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(sizeof(int) * count), elements, BufferUsageHint.StaticDraw);

            int size;
            GL.GetBufferParameter(BufferTarget.ElementArrayBuffer, BufferParameterName.BufferSize, out size);
            if (count * sizeof(int) != size)
                throw new ApplicationException("Element data not uploaded correctly");

            return new ElementBuffer(ebo, count);
        }
        #endregion

        #region Bind/Unbind
        public void Bind()
        {
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ebo);
        }
        public static void Unbind()
        {
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }
        #endregion

        #region Dispose
        public void Dispose()
        {
            Debug.Assert(ebo != 0);
            GL.DeleteBuffer(ebo);
            ebo = 0;
        }
        #endregion
    }
}
