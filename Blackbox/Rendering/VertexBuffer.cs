﻿using System;
using System.Diagnostics;
using Blackbox.Rendering.Shader;
using Blackbox.Rendering.VertexType;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Blackbox.Rendering
{
    public enum VertexBufferType
    {
        Static,
        Dynamic
    }

    public class VertexBuffer : IDisposable
    {
        private int vao;
        private int vbo;
        public int Count { get; private set;}

        private VertexBufferType type;

        #region .ctor
        private VertexBuffer(int vbo, int vao, int count, VertexBufferType type)
        {
            this.vbo = vbo;
            this.vao = vao;
            this.Count = count;
            this.type = type;
        }
        public static VertexBuffer CreateStatic<T>(T[] vertices, ShaderBase shader) where T : struct
        {
            int vao = GL.GenVertexArray();
            GL.BindVertexArray(vao);

            int count = vertices.Length;
            int stride = BlittableValueType.StrideOf(vertices);
            int vbo = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(stride * count), vertices, BufferUsageHint.StaticDraw);

            CheckUploadState(count, stride);
            VertexType.VertexTypes.SetupFunctionOf(vertices)(shader.ShaderProgram);
            GLHelper.CheckGLError();

            return new VertexBuffer(vbo, vao, count, VertexBufferType.Static);
        }

        public static VertexBuffer CreateDynamic<T>(T[] vertices, ShaderBase shader) where T : struct
        {
            int count = vertices.Length;
            int vao = GL.GenVertexArray();
            int vbo = GL.GenBuffer();

            GL.BindVertexArray(vao);
            GLHelper.CheckGLError();

            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GLHelper.CheckGLError();

            VertexTypes.SetupFunctionOf(vertices)(shader.ShaderProgram);
            GLHelper.CheckGLError();

            return new VertexBuffer(vbo, vao, count, VertexBufferType.Dynamic);
        }
        #endregion

        public void Bind<T>(T[] vertices) where T : struct
        {
            Debug.Assert(type == VertexBufferType.Dynamic);
            Debug.Assert(vertices.Length <= Count);

            int stride = BlittableValueType.StrideOf(vertices);

            GL.BindVertexArray(vao);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * stride), IntPtr.Zero, BufferUsageHint.StreamDraw);
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * stride), vertices, BufferUsageHint.StreamDraw);
        }

        #region Bind
        public void Bind()
        {
            GL.BindVertexArray(vao);
            GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
        }
        #endregion

        #region Dispose
        public void Dispose()
        {
            Debug.Assert(vbo != 0);
            GL.DeleteBuffer(vbo);
            vbo = 0;

            Debug.Assert(vao != 0);
            GL.DeleteVertexArray(vao);
            vao = 0;
        }
        #endregion

        [Conditional("DEBUG")]
        private static void CheckUploadState(int count, int stride)
        {
            int size;
            GL.GetBufferParameter(BufferTarget.ArrayBuffer, BufferParameterName.BufferSize, out size);
            if (count * stride != size)
                throw new ApplicationException("Vertex data not uploaded correctly");
        }
    }
}
