﻿using System;

namespace Blackbox.Rendering.VertexType
{
    public delegate void VertexSetupFunction(int shaderProgram);

    public static class VertexTypes
    {
        private static class VertexSetupFunctionHolder<T>
        {
            public readonly static VertexSetupFunction SetupFunction;
            static VertexSetupFunctionHolder()
            {
                var methodInfo = typeof(T).GetMethod("Setup");
                if (methodInfo == null)
                {
                    throw new ArgumentException("Method 'Setup' not found.");
                }
                SetupFunction = (VertexSetupFunction)Delegate.CreateDelegate(typeof(VertexSetupFunction), methodInfo);
            }
        }

        public static VertexSetupFunction SetupFunctionOf<T>(T[] type)
        {
            return VertexSetupFunctionHolder<T>.SetupFunction;
        }
    }
}
