﻿using Blackbox.Rendering.Shader;
using OpenTK;

namespace Blackbox.Rendering
{
    public struct RenderParameter
    {
        public Vector2 Position;
        public Vector2 Anchor;
        public Texture Texture;
        public ShaderBase ShaderType;

        public Vector2 Size;
        public Radian Angle;
        public Vector2 TextureUVMin;
        public Vector2 TextureUVMax;
    }
}
