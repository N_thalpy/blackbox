﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Blackbox.Rendering.Shader
{
    public class PixelateShader : ShaderBase
    {
        const String vertexSource = @"
                #version 150 core
                
                uniform mat3 trans;
                uniform vec2 viewPort;

                in vec2 positionIn;
                in vec4 colorIn;
                in vec2 textureIn;
                
                out vec4 color;
                out vec2 texCoord;
                
                void main()
                {
                    color = colorIn;
                    texCoord = textureIn;
                    vec3 transformed = trans * vec3(positionIn, 1);
                    transformed.x = transformed.x / viewPort.x * 2 - 1.0f;
                    transformed.y = transformed.y / viewPort.y * 2 - 1.0f;
                    gl_Position = vec4(transformed, 1);
                }
            ";

        const String fragmentSource = @"
                #version 150 core

                uniform sampler2D tex;

                in vec4 color;
                in vec2 texCoord;

                out vec4 colorOut;

                void main()
                {
                    float blockSize = 1; // TODO: decouple variable blockSize
                    float x = 1280 / blockSize;
                    float y = 760 / blockSize;

                    vec2 coord = vec2 (roundEven(texCoord.x * x) / x, roundEven(texCoord.y * y) / y); 
                    
                    colorOut = roundEven (texture(tex, coord) * color);     
                }
            ";

        public PixelateShader()
            : base(vertexSource, fragmentSource)
        {
        }

        protected override void GetUniformLocation()
        {
            transPtr = GL.GetUniformLocation(ShaderProgram, "trans");
            viewPortPtr = GL.GetUniformLocation(ShaderProgram, "viewPort");
        }

        public override void Bind(ref Matrix3 transform, Vector2 viewPort, Texture texture)
        {
            GL.UseProgram(ShaderProgram);

            GL.UniformMatrix3(transPtr, false, ref transform);
            GL.Uniform2(viewPortPtr, ref viewPort);
            GL.BindTexture(TextureTarget.Texture2D, texture.TextureHandle);
        }

        int transPtr;
        int viewPortPtr;
    }
}
