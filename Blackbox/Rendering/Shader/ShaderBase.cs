﻿using System;
using System.Diagnostics;
using System.IO;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Blackbox.Rendering.Shader
{
    public abstract class ShaderBase : IDisposable
    {
        private int vertexShader;
        private int fragmentShader;
        public int ShaderProgram { get; private set; }

        public ShaderBase(String vSource, String fSource)
        {
            CreateVertexShader(vSource);
            CreateFregmentShader(fSource);
            LinkShader();
            GetUniformLocation();
        }

        #region private Helpers
        private void CreateVertexShader(String source)
        {
            vertexShader = GL.CreateShader(ShaderType.VertexShader);
            CompileShader(vertexShader, source);
        }

        private void CreateFregmentShader(String source)
        {
            fragmentShader = GL.CreateShader(ShaderType.FragmentShader);
            CompileShader(fragmentShader, source);
        }

        private void CompileShader(int shader, String source)
        {
            String shaderLog;
            int shaderStatusCode;

            GL.ShaderSource(shader, source);
            GL.CompileShader(shader);
            GL.GetShaderInfoLog(shader, out shaderLog);
            GL.GetShader(shader, ShaderParameter.CompileStatus, out shaderStatusCode);

            if (shaderStatusCode == 0)
            {
                var stackTrace = new StackTrace(true);
                var stackFrames = stackTrace.GetFrame(4);
                var fileName = Path.GetFileNameWithoutExtension(stackFrames.GetFileName());
                throw new ArgumentException(String.Format("Shader compilation failed at '{0}'.\n{1}", fileName, shaderLog));
            }
        }

        private void LinkShader()
        {
            // TODO: Location Binding refactoring
            ShaderProgram = GL.CreateProgram();
            GL.AttachShader(ShaderProgram, vertexShader);
            GL.AttachShader(ShaderProgram, fragmentShader);
            GL.BindFragDataLocation(ShaderProgram, 0, "colorOut");
            GL.LinkProgram(ShaderProgram);
        }
        #endregion

        protected abstract void GetUniformLocation();
        public abstract void Bind(ref Matrix3 transform, Vector2 viewPort, Texture texture);

        public void Dispose()
        {
            if (vertexShader != 0) GL.DeleteShader(vertexShader); vertexShader = 0;
            if (fragmentShader != 0) GL.DeleteShader(fragmentShader); fragmentShader = 0;
            if (ShaderProgram != 0) GL.DeleteProgram(ShaderProgram); ShaderProgram = 0;
        }
    }
}
