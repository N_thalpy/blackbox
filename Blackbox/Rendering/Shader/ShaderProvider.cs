﻿namespace Blackbox.Rendering.Shader
{
    public static class ShaderProvider
    {
        public static ScreenSpaceTextureShader ScreenSpaceTexture { get; private set; }
        public static PixelateShader PixelateShader { get; private set; }

        static ShaderProvider()
        {
            ScreenSpaceTexture = new ScreenSpaceTextureShader();
            PixelateShader = new PixelateShader();
        }
    }
}
