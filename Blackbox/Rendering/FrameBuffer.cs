﻿using System;
using System.Drawing;
using Blackbox.BlackboxSystem;
using Blackbox.Rendering.Shader;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Blackbox.Rendering
{
    public class FrameBuffer
    {
        private int Handle;
        private Texture texture;
        private int depthBuffer;

        private FrameBuffer(int frame, int tHandle, int rbHandle, int width, int height)
        {
            Handle = frame;
            depthBuffer = rbHandle;

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, Handle);
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, depthBuffer);
            GL.BindTexture(TextureTarget.Texture2D, tHandle);

            GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent, width, height);
            GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, depthBuffer);
            GLHelper.CheckGLError();

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0, PixelFormat.Bgra, PixelType.UnsignedByte, new IntPtr());
            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, tHandle, 0);
            GLHelper.CheckGLError();

            var errorCode = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
            if (errorCode != FramebufferErrorCode.FramebufferComplete)
                throw new Exception(errorCode.ToString());

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, 0);
            GL.BindTexture(TextureTarget.Texture2D, 0);
            texture = Texture.Create(tHandle, width, height);
        }

        public static FrameBuffer Create(int width, int height)
        {
            return new FrameBuffer(GL.GenFramebuffer(), GL.GenTexture(), GL.GenRenderbuffer(), width, height);
        }

        public void Dispose()
        {
            GL.DeleteFramebuffer(Handle);
        }

        public void Bind()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, Handle);
            GLHelper.CheckGLError();
        }
        public void Unbind()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }
        public void Flush()
        {
            Size clientSize = GameSystem.MainForm.ClientSize;
            Vector2 viewport = GameSystem.MainForm.Viewport;
            Vector2 size = new Vector2(
                clientSize.Width * clientSize.Width / viewport.X,
                clientSize.Height * clientSize.Height / viewport.Y);

            GLHelper.DrawTexture(new RenderParameter()
            {
                Texture = texture,
                Anchor = Vector2.One / 2,
                Position = size / 2,
                ShaderType = ShaderProvider.PixelateShader,

                Size = size,
                Angle = Radian.Zero,
                TextureUVMin = Vector2.Zero,
                TextureUVMax = Vector2.One
            });
        }
    }
}
