﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Imaging = System.Drawing.Imaging;

namespace Blackbox.Rendering
{
    public class Texture : IDisposable
    {
        public int TextureHandle { get; private set; }
        public Vector2 Size { get; private set; }

        private Texture(int handle)
        {
            TextureHandle = handle;
        }

        public static Texture Create(int handle, int width, int height)
        {
            Texture rv = new Texture(handle);
            rv.Size = new Vector2(width, height);

            GL.BindTexture(TextureTarget.Texture2D, handle);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

            GL.BindTexture(TextureTarget.Texture2D, 0);

            return rv;
        }
        public static Texture CreateFromBitmap(Bitmap bitmap)
        {
            int id = GL.GenTexture();
            Debug.Assert(id != 0);
            GL.BindTexture(TextureTarget.Texture2D, id);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly, Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bitmapData.Width, bitmapData.Height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bitmapData.Scan0);
            bitmap.UnlockBits(bitmapData);

            GL.BindTexture(TextureTarget.Texture2D, 0);

            Texture tex = new Texture(id);
            tex.Size = new Vector2(bitmap.Width, bitmap.Height);
            return tex;
        }
        public static Texture CreateFromBitmapPath(String path)
        {
            if (File.Exists(path) == false)
                throw new FileNotFoundException(String.Format("File {0} has not found.", path));

            using (Bitmap bitmap = new Bitmap(path))
            {
                return CreateFromBitmap(new Bitmap(path));
            }
        }

        public void Dispose()
        {
            GL.DeleteTexture(TextureHandle);
        }
    }
}
