﻿namespace Blackbox.Rendering.BitmapFont
{
    public static class BitmapFontProvider
    {
        public static readonly BitmapFont Font;

        static BitmapFontProvider()
        {
            Font = BitmapFont.Create("Resource/font.fnt");
        }
    }
}
