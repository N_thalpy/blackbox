﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Blackbox.BlackboxSystem;
using OpenTK.Input;
using Blackbox.Sound;
using System;

namespace Blackbox.Input
{
    public enum InputType
    {
        None,
        Dot,
        Dash,
        LongDash,
    }

    public static class InputHelper
    {
        private enum KeyState
        {
            None,
            Pressed,
        }
        private static KeyState state;
        private static Stopwatch sw;

        private static KeyboardState prevState;
        private static IEnumerable<Key> keyList;

        private static InputType output;

        static InputHelper()
        {
            state = KeyState.None;
            sw = new Stopwatch();
            output = InputType.None;

            prevState = Keyboard.GetState();

            List<int> tempKeyList = new List<int>();
            for (int idx = 0; idx < (int)Key.LastKey; idx++)
                tempKeyList.Add(idx);
            keyList = tempKeyList.Select(_ => (Key)_);
        }

        public static void Update()
        {
            if (GameSystem.MainForm.Focused == false)
                return;
            
            KeyboardState currState = Keyboard.GetState();
            if (keyList.Any(_ => prevState[_] == false && currState[_] == true) && state == KeyState.None)
                Down();
            else if (keyList.All(_ => currState[_] == false) && keyList.Any(_ => prevState[_] == true) && state == KeyState.Pressed)
                Up();
            else
            {
                switch (state)
                {
                    case KeyState.None:
                        SetOutput(InputType.None);
                        break;

                    case KeyState.Pressed:
                        Second duration = (Second)((float)sw.ElapsedTicks / Stopwatch.Frequency);
                        if (duration < Config.DotMaxDuration)
                            SetOutput(InputType.Dot);
                        else if (duration < Config.DashMaxDuration)
                            SetOutput(InputType.Dash);
                        else
                            SetOutput(InputType.LongDash);
                        break;
                }
            }

            prevState = currState;
        }

        public static InputType GetKey()
        {
            switch (state)
            {
                case KeyState.Pressed:
                    return InputType.None;

                case KeyState.None:
                    return output;

                default:
                    throw new ArgumentException(String.Format("Unknown State {0}", state));
            }
        }

        private static void SetOutput(InputType type)
        {
            if (output != type)
                switch (type)
                {
                    case InputType.Dot:
                        ALHelper.PlayBeep(220);
                        break;

                    case InputType.Dash:
                        ALHelper.PlayBeep(330);
                        break;

                    case InputType.LongDash:
                        ALHelper.PlayBeep(440);
                        break;
                }

            output = type;
        }
        private static void Down()
        {
            Debug.Assert(state == KeyState.None);
            state = KeyState.Pressed;

            sw.Restart();
        }
        private static void Up()
        {
            Debug.Assert(state == KeyState.Pressed);
            ALHelper.Stop();
            state = KeyState.None;

            sw.Stop();
        }
    }
}
