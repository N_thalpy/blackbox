﻿using System;
using Blackbox.BlackboxSystem;
using Blackbox.GameLoop.Impl;

namespace Blackbox
{
    public static class Program
    {
        [method: STAThread]
        public static void Main()
        {
            GameSystem.Initialize(1280, 760, "Blackbox");

            GameSystem.PushGameLoop(new ExitLoop());
            GameSystem.PushGameLoop(new BootLoop());

            GameSystem.MainForm.Run(60);
        }
    }
}
