﻿using OpenTK.Input;
using System;
using System.Diagnostics;

namespace Blackbox.BlackboxSystem
{
    public static class Debugger
    {
        public static bool RenderingDebug
        {
            get;
            private set;
        }
        private static bool pressed;

        [method: Conditional("DEBUG")]
        public static void Update()
        {
            if (GameSystem.MainForm.Focused == true)
            {
                MouseState state = Mouse.GetState(0);
                if (pressed == false && state.IsButtonDown(MouseButton.Right) == true)
                {
                    RenderingDebug ^= true;
                    pressed = true;
                }

                if (pressed == true && state.IsButtonUp(MouseButton.Right) == true)
                    pressed = false;
            }
        }

        [method: Conditional("DEBUG")]
        public static void Alert(Exception e)
        {
            Console.WriteLine(e);
        }
    }
}
