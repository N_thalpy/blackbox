﻿using Blackbox.GameObject.Logic;
using Blackbox.GameObject.Logic.Automata;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace Blackbox.BlackboxSystem
{
    public static class UserData
    {
        public enum ProblemState
        {
            Unknown,
            NotSolved,
            Solved,
        };

        private const String FilePath = "save.dat";

        private static Dictionary<String, ProblemState> problemStateDict;
        private static BinaryFormatter bf;

        static UserData()
        {
            problemStateDict = new Dictionary<String, ProblemState>();
            bf = new BinaryFormatter();
        }
        
        public static void Initialize()
        {
            Load();
        }
        public static ProblemState GetState(AutomataBase a)
        {
            String name = a.Name;
            if (problemStateDict.ContainsKey(name) == false)
            {
                problemStateDict.Add(name, ProblemState.NotSolved);
                Save();
            }

            return problemStateDict[name];
        }
        public static void SetState(AutomataBase a, ProblemState state)
        {
            problemStateDict[a.Name] = state;
            Save();
        }

        private static void Load()
        {
            try
            {
                using (StreamReader sr = new StreamReader(FilePath))
                    problemStateDict = bf.Deserialize(sr.BaseStream) as Dictionary<String, ProblemState>;
            }
            catch (Exception e)
            {
                Debugger.Alert(e);

                File.Create(FilePath).Close();
                Save();
            }
        }
        private static void Save()
        {
            using (StreamWriter sw = new StreamWriter(FilePath))
                bf.Serialize(sw.BaseStream, problemStateDict);
        }
    }
}
