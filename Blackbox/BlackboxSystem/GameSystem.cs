﻿using System;
using System.Collections.Generic;
using Blackbox.GameLoop;
using Blackbox.Input;

namespace Blackbox.BlackboxSystem
{
    public static class GameSystem
    {
        public static MainForm MainForm { get; private set; }

        private static GameTime UpdateTime;
        private static GameTime RenderTime;

        private static Stack<GameLoopBase> GameLoopStack;

        static GameSystem()
        {
            GameLoopStack = new Stack<GameLoopBase>();
        }

        public static void Initialize(int width, int height, String windowName)
        {
            MainForm = new MainForm(width, height, windowName);

            UpdateTime = new GameTime();
            RenderTime = new GameTime();
        }

        public static void PushGameLoop(GameLoopBase gameLoop)
        {
            gameLoop.ResetInitializeState();
            GameLoopStack.Push(gameLoop);
        }
        public static void PopGameLoop()
        {
            GameLoopStack.Pop();
        }

        public static void UpdateTick()
        {
            GameLoopBase gl = GameLoopStack.Peek();
            Second deltaTime = UpdateTime.DeltaTime;
            UpdateTime.Refresh();

            Debugger.Update();
            InputHelper.Update();

            gl.Initialize();
            gl.Update(deltaTime);
        }
        public static void RenderTick()
        {
            GameLoopBase gl = GameLoopStack.Peek();
            Second deltaTime = RenderTime.DeltaTime;
            RenderTime.Refresh();

            gl.Initialize();
            gl.Render(deltaTime);
        }
    }
}
