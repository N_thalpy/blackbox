﻿using Blackbox.Input;
using System.Collections.Generic;

namespace Blackbox.BlackboxSystem
{
    public static class Config
    {
        public static readonly Second DotMaxDuration;
        public static readonly Second DashMaxDuration;

        public static readonly int Revision;
        public static readonly int PasswordLength;

        static Config()
        {
            DotMaxDuration = (Second)0.25f;
            DashMaxDuration = (Second)1f;

            Revision = 122;
            PasswordLength = 9;
        }

        public static bool CheckPassword(List<InputType> pw)
        {
            // TODO: Implement Password Authorizing
            return true;
        }
    }
}
