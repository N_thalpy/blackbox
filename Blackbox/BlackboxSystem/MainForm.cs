﻿using System;
using Blackbox.Rendering;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Blackbox.Sound;
using System.Drawing;

namespace Blackbox.BlackboxSystem
{
    public class MainForm : GameWindow
    {
        public Vector2 Viewport;
        public FrameBuffer FrameBufferObject;

        public int width;
        public int height;

        public MainForm(int width, int height, String name)
            : base(width, height, new GraphicsMode(32, 24, 0, 4), name,
                  GameWindowFlags.FixedWindow, DisplayDevice.Default,
                  3, 2, GraphicsContextFlags.ForwardCompatible)
        {
            this.width = width;
            this.height = height;

            WindowState = WindowState.Fullscreen;
            Viewport = new Vector2(width, height);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            ALHelper.Initialize();
        }
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
            GameSystem.UpdateTick();
        }
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            if (FrameBufferObject == null)
                FrameBufferObject = FrameBuffer.Create(width, height);

            FrameBufferObject.Bind();
            {
                GL.ClearColor(Color4.Transparent);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

                GL.Viewport(0, 0, width, height);
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                GLHelper.CheckGLError();

                GameSystem.RenderTick();
                GL.Disable(EnableCap.Blend);
            }
            FrameBufferObject.Unbind();
            GLHelper.CheckGLError();

            GL.Viewport(0, 0, (int)ClientSize.Width, (int)ClientSize.Height);
            if (Debugger.RenderingDebug == true)
                GL.ClearColor(Color4.Crimson);
            else
                GL.ClearColor(Color4.Black);
            
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            FrameBufferObject.Flush();
            GLHelper.CheckGLError();
            GL.Disable(EnableCap.Blend);

            SwapBuffers();
        }
    }
}
