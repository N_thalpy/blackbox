﻿using System.Diagnostics;

namespace Blackbox.BlackboxSystem
{
    public class GameTime
    {
        #region public Variables
        public Second DeltaTime { get; private set; }
        public long Frame { get; private set; }
        #endregion
        #region private Variables
        private Stopwatch sw;

        private long currentTick;
        private long oldTick;
        #endregion

        #region .ctor
        public GameTime()
        {
            sw = new Stopwatch();
            sw.Start();

            currentTick = sw.ElapsedTicks;
            oldTick = currentTick;
            Frame = 0;
        }
        #endregion

        public void Reset()
        {
            currentTick = sw.ElapsedTicks;
            oldTick = currentTick;
            Frame = 0;
        }
        public void Refresh()
        {
            oldTick = currentTick;
            currentTick = sw.ElapsedTicks;

            Debug.Assert(oldTick < currentTick);
            DeltaTime = (Second)((float)(currentTick - oldTick) / Stopwatch.Frequency);
            Frame++;
        }
    }
}
