﻿using System;
using System.Drawing;
using Blackbox.Rendering;
using Blackbox.Rendering.Shader;
using OpenTK;

namespace Blackbox.GameObject.Renderable
{
    public sealed class Circle : IRenderable, IDisposable
    {
        public Vector2 Position;
        public bool Visible { get; set; }

        private Texture tex;

        public Circle(float r, float thickness, Color borderColor, Color innerColor)
        {
            int width = (int)Math.Floor((r + thickness) * 2) + 1;
            int height = (int)Math.Floor((r + thickness) * 2) + 1;
            Visible = true;

            using (Bitmap b = new Bitmap(width, height))
            {
                for (int x = 0; x < width; x++)
                    for (int y = 0; y < height; y++)
                    {
                        int dx = x - width / 2;
                        int dy = y - height / 2;

                        if (Math.Abs(Math.Sqrt(dx * dx + dy * dy) - r) < thickness / 2)
                            b.SetPixel(x, y, borderColor);
                        else if (Math.Sqrt(dx * dx + dy * dy) < r - thickness / 2)
                            b.SetPixel(x, y, innerColor);
                        else
                            b.SetPixel(x, y, Color.Transparent);
                    }

                tex = Texture.CreateFromBitmap(b);
            }
        }

        public void Render()
        { 
            if (Visible == false)
                return;

            GLHelper.DrawTexture(new RenderParameter()
            {
                Texture = tex,
                Anchor = Vector2.One / 2,
                Position = Position,
                ShaderType = ShaderProvider.ScreenSpaceTexture,

                Size = tex.Size,
                Angle = Radian.Zero,
                TextureUVMin = Vector2.Zero,
                TextureUVMax = Vector2.One,

            });
        }

        public void Dispose()
        {
            tex.Dispose();
        }
    }
}
