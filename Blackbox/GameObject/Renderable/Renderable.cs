﻿namespace Blackbox.GameObject.Renderable
{
    public interface IRenderable
    {
        bool Visible { get; set; }
        void Render();
    }
}
