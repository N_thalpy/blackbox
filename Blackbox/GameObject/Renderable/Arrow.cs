﻿using System;
using System.Diagnostics;
using System.Drawing;
using Blackbox.Rendering;
using Blackbox.Rendering.Shader;
using OpenTK;

namespace Blackbox.GameObject.Renderable
{
    public class Arrow : IRenderable, IDisposable
    {
        public Vector2 Position;
        public float Length;
        public Radian Angle;
        public float Anchor;

        private Texture barTex;
        private Texture triangleTex;

        public bool Visible { get; set; }

        public Arrow(int width, int tipWidth, Color c)
        {
            Debug.Assert(tipWidth > width);
            Visible = true;

            using (Bitmap b = new Bitmap(16, width))
            {
                for (int x = 0; x < 16; x++)
                    for (int y = 0; y < width; y++)
                        b.SetPixel(x, y, c);
                
                barTex = Texture.CreateFromBitmap(b);
            }

            using (Bitmap b = new Bitmap(tipWidth, tipWidth))
            {
                for (int x = 0; x < tipWidth; x++)
                    for (int y = 0; y < tipWidth; y++)
                    {
                        if (tipWidth - x > Math.Abs(y - tipWidth / 2) * 2)
                            b.SetPixel(x, y, c);
                        else
                            b.SetPixel(x, y, Color.Transparent);
                    }

                triangleTex = Texture.CreateFromBitmap(b);
            }
        }

        public void Render()
        {
            if (Visible == false)
                return;

            Vector2 dir = new Vector2((float)Math.Cos((float)Angle), (float)Math.Sin((float)Angle));

            GLHelper.DrawTexture(new RenderParameter()
            {
                Texture = barTex,
                Anchor = new Vector2(0, 0.5f),
                Position = Position - dir * Anchor,
                ShaderType = ShaderProvider.ScreenSpaceTexture,

                Size = new Vector2(Length, barTex.Size.Y),
                Angle = Angle,
                TextureUVMin = Vector2.Zero,
                TextureUVMax = Vector2.One,
            });
            GLHelper.DrawTexture(new RenderParameter()
            {
                Texture = triangleTex,
                Anchor = new Vector2(0.5f, 0.5f),
                Position = Position + dir * (Length - Anchor),
                ShaderType = ShaderProvider.ScreenSpaceTexture,

                Size = triangleTex.Size,
                Angle = Angle,
                TextureUVMin = Vector2.Zero,
                TextureUVMax = Vector2.One,
            });
        }

        public void Dispose()
        {
            barTex.Dispose();
            triangleTex.Dispose();
        }
    }
}
