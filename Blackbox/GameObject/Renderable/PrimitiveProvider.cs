﻿using System.Drawing;

namespace Blackbox.GameObject.Renderable
{
    public static class PrimitiveProvider
    {
        public static Circle RedCircle { get; private set; }
        public static Circle YellowCircle { get; private set; }
        public static Circle BlueCircle { get; private set; }
        public static Circle GreenCircle { get; private set; }
        public static Circle TransparentCircle { get; private set; }

        public static Arrow RedArrow { get; private set;}

        static PrimitiveProvider()
        {
            Initialize();
        }
        public static void Initialize()
        {
            float r = 60;
            float thickness = 10;

            int arrowWidth = 16;
            int arrowTipWidth = 64;

            RedCircle = new Circle(r, thickness, Color.White, Color.Red);
            YellowCircle = new Circle(r, thickness, Color.White, Color.Yellow);
            BlueCircle = new Circle(r, thickness, Color.White, Color.Blue);
            GreenCircle = new Circle(r, thickness, Color.White, Color.Green);
            TransparentCircle = new Circle(r, thickness, Color.White, Color.Transparent);

            RedArrow = new Arrow(arrowWidth, arrowTipWidth, Color.Red);
        }
    }
}
