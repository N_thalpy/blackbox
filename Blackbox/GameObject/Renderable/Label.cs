﻿using System;
using System.Drawing;
using Blackbox.Rendering;
using Blackbox.Rendering.BitmapFont;
using OpenTK;
using Blackbox.Rendering.Shader;

namespace Blackbox.GameObject.Renderable
{
    public class Label : IRenderable
    {
        public Vector2 Anchor;
        public Vector2 Position;
        public String Text;
        public float FontScale;

        public bool Visible { get; set; }

        private BitmapFont font;
        private Texture tex;

        public Label(BitmapFont font)
        {
            Visible = true;
            Text = String.Empty;
            Anchor = Vector2.One / 2;

            this.font = font;
            this.FontScale = 2;
            this.tex = Texture.CreateFromBitmapPath(font.Pages[0].FileName);
        }

        public void Render()
        {
            if (Visible == false)
                return;

            Size measure = font.MeasureFont(Text);
            Vector2 initpos = Position + new Vector2(-Anchor.X * measure.Width, (1 - Anchor.Y) * measure.Height) * FontScale;
            Vector2 pos = initpos;

            for (int idx = 0; idx < Text.Length; idx++)
            {
                Char s = Text[idx];
                int kern = 0;
                if (idx != Text.Length - 1)
                    font.GetKerning(Text[idx], Text[idx + 1]);
                
                switch (s)
                {
                    case '\n':
                        pos.X = initpos.X;
                        pos.Y -= font.LineHeight * FontScale;
                        break;

                    case '\r':
                        // Do Nothing
                        break;

                    default:
                        Character c = font.Characters[s];
                        GLHelper.DrawTexture(new RenderParameter()
                        {
                            Position = pos + new Vector2((c.Offset.X + kern), -c.Offset.Y) * FontScale,
                            Anchor = new Vector2(0, 1),
                            Texture = tex,
                            ShaderType = ShaderProvider.ScreenSpaceTexture,

                            Size = new Vector2(c.Bounds.Width, c.Bounds.Height) * FontScale,
                            Angle = Radian.Zero,
                            TextureUVMin = new Vector2(c.Bounds.X / tex.Size.X, (c.Bounds.Y + c.Bounds.Height) / tex.Size.Y),
                            TextureUVMax = new Vector2((c.Bounds.X + c.Bounds.Width) / tex.Size.X, c.Bounds.Y / tex.Size.Y)
                        });
                        pos.X += (c.XAdvance + kern) * FontScale;
                        break;
                }
            }
        }
    }
}
