﻿using Blackbox.GameObject.Logic.Constraint;
using Blackbox.GameObject.Renderable;
using Blackbox.Input;
using OpenTK;
using System;

namespace Blackbox.GameObject.Logic.Automata
{
    public abstract class AutomataBase : IRenderable
    {
        public Vector2 Position;
        public bool Visible { get; set; }
        public readonly IConstraint Constraint;
        public readonly String Name;

        private bool isDirty;

        protected abstract void OnPushSymbol(Symbol symbol);
        protected abstract bool OnAccepted();
        protected abstract void OnReset();
        protected abstract void OnRender();
                
        public AutomataBase(IConstraint constraint, String name)
        {
            Visible = true;
            Name = name;

            this.isDirty = false;
            this.Constraint = constraint;
        }

        public void Render()
        {
            if (Visible == false)
                return;

            OnRender();
        }

        public void PushSymbol(Symbol symbol)
        {
            Constraint.PushSymbol();
            OnPushSymbol(symbol);
        }
        public bool Accepted()
        {
            return Constraint.IsValid() && OnAccepted();
        }
        public void Reset()
        {
            isDirty = false;
            Constraint.Reset();
            OnReset();
        }
        public void Update()
        {
            InputType k = InputHelper.GetKey();
            switch (k)
            {
                case InputType.Dot:
                    isDirty = true;
                    PushSymbol(Symbol.Dot);
                    break;

                case InputType.Dash:
                    isDirty = true;
                    PushSymbol(Symbol.Dash);
                    break;

                case InputType.LongDash:
                    Reset();
                    isDirty = false;
                    break;

                default:
                    break;
            }
        }

        public bool IsDirty()
        {
            return isDirty;
        }
    }
}
