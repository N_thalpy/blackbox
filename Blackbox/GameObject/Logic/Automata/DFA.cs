﻿using Blackbox.GameObject.Logic.Constraint;
using Blackbox.GameObject.Renderable;
using Blackbox.Rendering.BitmapFont;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Blackbox.GameObject.Logic.Automata
{
    public class DFA : AutomataBase
    {
        public class Element
        {
            public Vector2 Position;
            public Dictionary<Symbol, Element> Transition;

            public Element()
            {
                Transition = new Dictionary<Symbol, Element>();
                Transition.Add(Symbol.Dot, null);
                Transition.Add(Symbol.Dash, null);
            }
        }
        
        private Element start;
        private List<Element> elements;
        private IEnumerable<Element> end;

        private Element currentElement;
        private bool isValid;

        private Label lb;

        private Vector2 anchor;

        public DFA(Element start, IEnumerable<Element> end, IConstraint constraint, String name)
            : base(constraint, name) 
        {
            this.start = start;
            this.end = end;
            this.currentElement = start;
            this.isValid = true;
            
            lb = new Label(BitmapFontProvider.Font);

            elements = new List<Element>();
            Queue<Element> elementQueue = new Queue<Element>();
            elementQueue.Enqueue(start);
            elements.Add(start);

            while (elementQueue.Count != 0)
            {
                Element e = elementQueue.Dequeue();
                
                foreach (Element v in e.Transition.Values)
                    if (v != null && elements.Contains(v) == false)
                    {
                        elements.Add(v);
                        elementQueue.Enqueue(v);
                    }
            }

            float minx = Single.MaxValue, maxx = Single.MinValue;
            float miny = Single.MaxValue, maxy = Single.MinValue;
            foreach (Element e in elements)
            {
                minx = Math.Min(minx, e.Position.X);
                maxx = Math.Max(maxx, e.Position.X);
                miny = Math.Min(miny, e.Position.Y);
                maxy = Math.Max(maxy, e.Position.Y);
            }

            anchor = new Vector2(maxx + minx, maxy + miny) / 2;
        }

        protected override void OnReset()
        {
            currentElement = start; 
            isValid = true;
        }
        protected override void OnPushSymbol(Symbol symbol)
        {
            if (isValid == true)
            {
                Element next = currentElement.Transition[symbol];

                if (next == null)
                    isValid = false;
                else
                    currentElement = next;
            }
        }
        protected override bool OnAccepted()
        {
            return end.Contains(currentElement);
        }

        protected override void OnRender()
        {
            Vector2 pos = Position - anchor / 2;
            foreach (Element e in elements)
            {
                Circle c;
                if (currentElement == e)
                {
                    if (isValid == true)
                        c = PrimitiveProvider.YellowCircle;
                    else
                        c = PrimitiveProvider.RedCircle;
                }
                else if (end.Contains(e))
                    c = PrimitiveProvider.GreenCircle;
                else
                    c = PrimitiveProvider.TransparentCircle;

                c.Position = e.Position + pos;
                c.Render();
            }

            foreach (Element src in elements)
                foreach (Symbol key in src.Transition.Keys)
                {
                    Element dst = src.Transition[key];
                    if (dst != null)
                    {
                        Arrow a = PrimitiveProvider.RedArrow;
                        Vector2 dp = dst.Position - src.Position;

                        Vector2 orth = Vector2.Normalize(new Vector2(-dp.Y, dp.X));
                        a.Position = pos + (dst.Position + src.Position) / 2 + orth * 35;

                        // TODO: value decoupling
                        a.Length = dp.Length - 120;
                        a.Anchor = a.Length / 2;
                        a.Angle = (Radian)(Math.Atan2(dp.Y, dp.X));

                        a.Render();

                        lb.Position = pos + (dst.Position + src.Position) / 2 + orth * 50;
                        lb.Text = key == Symbol.Dot ? "d" : "D";
                        lb.Anchor = Vector2.One / 2;

                        lb.Render();
                    }
                }
        }
    }
}
