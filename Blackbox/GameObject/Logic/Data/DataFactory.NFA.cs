﻿using Blackbox.GameObject.Logic.Automata;
using Blackbox.GameObject.Logic.Constraint;
using OpenTK;
using System;
using static Blackbox.GameObject.Logic.Automata.NFA;

namespace Blackbox.GameObject.Logic.Data
{
    public static partial class DataFactory
    {
        public static NFA NFA0;

        private static void InitializeNFA0()
        {
            Element a = new Element();

            Element b1 = new Element();
            Element c1 = new Element();
            Element d1 = new Element();

            Element b2 = new Element();
            Element c2 = new Element();

            Element e = new Element();

            a.Transition[Symbol.Dot].Add(b1);
            a.Transition[Symbol.Dot].Add(b2);

            b1.Transition[Symbol.Dot].Add(c1);
            c1.Transition[Symbol.Dot].Add(d1);
            d1.Transition[Symbol.Dot].Add(b1);

            b2.Transition[Symbol.Dot].Add(c2);
            c2.Transition[Symbol.Dot].Add(b2);

            b1.Transition[Symbol.Dash].Add(e);
            b2.Transition[Symbol.Dash].Add(e);

            float r1 = 400;
            float r2 = 200;

            a.Position = new Vector2(-r1, 0);
            e.Position = new Vector2(r1, 0);

            b1.Position = new Vector2(0, r2 / 2);
            c1.Position = new Vector2(-r2, r2 / 2 * (1 + (float)Math.Sqrt(3)));
            d1.Position = new Vector2(r2, r2 / 2 * (1 + (float)Math.Sqrt(3)));

            b2.Position = new Vector2(0, -r2 / 2);
            c2.Position = new Vector2(0, -r2 * 1.5f);

            NFA0 = new NFA(a, new Element[] { e }, new NullConstraint(), "NFA T0");
        }
    }
}
