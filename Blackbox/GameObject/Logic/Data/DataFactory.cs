﻿using System;
using System.Reflection;

namespace Blackbox.GameObject.Logic.Data
{
    public static partial class DataFactory
	{
		static DataFactory()
		{
            foreach (MethodInfo m in typeof(DataFactory).GetMethods(BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.InvokeMethod))
                m.Invoke(null, null);
		}
	}
}
