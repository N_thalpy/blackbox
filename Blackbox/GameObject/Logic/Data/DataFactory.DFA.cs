﻿using Blackbox.GameObject.Logic.Automata;
using Blackbox.GameObject.Logic.Constraint;
using OpenTK;
using System;

namespace Blackbox.GameObject.Logic.Data
{
    public static partial class DataFactory
    {
        public static DFA Tutorial0;
        public static DFA Tutorial1;
        public static DFA Tutorial2;

        private static void InitializeDFA0()
        {
            DFA.Element e0 = new DFA.Element();
            DFA.Element e1 = new DFA.Element();
            DFA.Element e2 = new DFA.Element();
            DFA.Element e3 = new DFA.Element();
            DFA.Element e4 = new DFA.Element();

            float r = 200;
            e0.Position = new Vector2(-2 * r, 0);
            e1.Position = new Vector2(-r, 0);
            e2.Position = new Vector2(0, 0);
            e3.Position = new Vector2(r, 0);
            e4.Position = new Vector2(2 * r, 0);

            e0.Transition[Symbol.Dot] = e1;
            e1.Transition[Symbol.Dash] = e2;
            e2.Transition[Symbol.Dot] = e3;
            e3.Transition[Symbol.Dot] = e4;

            Tutorial0 = new DFA(e0, new DFA.Element[] { e4 }, new NullConstraint(), "T0");
        }
        private static void InitializeDFA1()
        {
            DFA.Element e0 = new DFA.Element();
            DFA.Element e1 = new DFA.Element();
            DFA.Element e2 = new DFA.Element();

            float r = 200;
            e0.Position = new Vector2(-r, 0);
            e1.Position = new Vector2(0, r * (float)Math.Sqrt(3));
            e2.Position = new Vector2(r, 0);

            e0.Transition[Symbol.Dot] = e1;
            e1.Transition[Symbol.Dot] = e2;
            e2.Transition[Symbol.Dot] = e1;
            e2.Transition[Symbol.Dash] = e0;

            Tutorial1 = new DFA(e0, new DFA.Element[] { e2 }, new EqualLengthConstraint(7), "T1");
        }
        private static void InitializeDFA2()
        {
            DFA.Element e0 = new DFA.Element();
            DFA.Element e1 = new DFA.Element();
            DFA.Element e2 = new DFA.Element();
            DFA.Element e3 = new DFA.Element();

            float r = 200;
            e0.Position = new Vector2(-r, 0);
            e1.Position = new Vector2(0, -r);
            e2.Position = new Vector2(r, 0);
            e3.Position = new Vector2(0, r);

            e0.Transition[Symbol.Dot] = e1;
            e1.Transition[Symbol.Dot] = e2;
            e2.Transition[Symbol.Dot] = e3;
            e3.Transition[Symbol.Dot] = e0;

            e0.Transition[Symbol.Dash] = e3;
            e1.Transition[Symbol.Dash] = e0;
            e2.Transition[Symbol.Dash] = e1;
            e3.Transition[Symbol.Dash] = e2;

            Tutorial2 = new DFA(e0, new DFA.Element[] { e2 }, new EqualLengthConstraint(4), "T2");
        }
    }
}
