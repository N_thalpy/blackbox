﻿using System;

namespace Blackbox.GameObject.Logic.Constraint
{
    public interface IConstraint
    {
        bool IsValid();
        void PushSymbol();
        void Reset();
        
        String GetConstraintString();
        String GetCurrentStateString();
    }
}