﻿using System;

namespace Blackbox.GameObject.Logic.Constraint
{
    public sealed class NullConstraint : IConstraint
    {
        public NullConstraint()
        {
        }

        public string GetConstraintString()
        {
            return String.Format("L: N");
        }
        public string GetCurrentStateString()
        {
            return String.Empty;
        }

        public bool IsValid()
        {
            return true;
        }
        public void PushSymbol()
        {
        }
        public void Reset()
        {
        }
    }
}
