﻿using System;

namespace Blackbox.GameObject.Logic.Constraint
{
    public sealed class EqualLengthConstraint : IConstraint
    {
        private int limit;
        private int current;

        public EqualLengthConstraint(int limit)
        {
            this.limit = limit;
            this.current = 0;
        }

        public string GetConstraintString()
        {
            return String.Format("L: Eq {0}", limit);
        }
        public string GetCurrentStateString()
        {
            return String.Format("C: {0}", current);
        }

        public bool IsValid()
        {
            return limit == current;
        }
        public void PushSymbol()
        {
            current++;
        }
        public void Reset()
        {
            current = 0;
        }
    }
}
