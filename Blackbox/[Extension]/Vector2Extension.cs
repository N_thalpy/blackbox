﻿using System;
using OpenTK;

namespace Blackbox
{
    public static class Vector2Extension
    {
        public static Vector2 Rotate(this Vector2 v, Radian angle)
        {
            float cos = (float)Math.Cos((float)angle);
            float sin = (float)Math.Sin((float)angle);

            return new Vector2(v.X * cos - v.Y * sin, v.X * sin + v.Y * cos);
        }
    }
}
