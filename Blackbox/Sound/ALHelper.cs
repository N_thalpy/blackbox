﻿using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
using System;
using System.Diagnostics;

namespace Blackbox.Sound
{
    public static class ALHelper
    {
        private static AudioContext ac;
        private static int buffer;
        private static int source;

        public static void CheckALError()
        {
            var error = AL.GetError();
            Debug.Assert(error == ALError.NoError);

            if (error != ALError.NoError)
                throw new Exception(AL.GetErrorString(error));
        }

        public static void Initialize()
        {
            ac = new AudioContext();

            AL.Listener(ALListenerf.Gain, 10);

            buffer = AL.GenBuffer();
            source = AL.GenSource();

            int sampleRate = 44100;
            int hz = 440;
            int length = 1;
            Int16[] sample = new Int16[sampleRate * length];
            
            for (int i = 0; i < sample.Length; i++)
                sample[i] = (Int16)(Int16.MaxValue * Math.Sin(2 * Math.PI / sampleRate * i * hz));

            AL.BufferData(buffer, ALFormat.Mono16, sample, sample.Length, sampleRate);
            ALHelper.CheckALError();

            AL.BindBufferToSource(source, buffer);
            AL.Source(source, ALSourcef.Gain, 0.1f);
            AL.Source(source, ALSourcef.Pitch, 1);
            AL.Source(source, ALSourceb.Looping, true);
            ALHelper.CheckALError();
        }

        public static void PlayBeep(float hz)
        {
            AL.Source(source, ALSourcef.Pitch, hz / 440);
            AL.SourcePlay(source);
            ALHelper.CheckALError();
        }

        public static void Stop()
        {
            AL.SourceStop(source);
        }
    }
}
