﻿using OpenTK;
using System;

namespace Blackbox
{
    [Serializable]
    public struct Radian
    {
        private readonly float InnerValue;

        #region static
        public static Radian Zero
        {
            get
            {
                return (Radian)0f;
            }
        }
        public static Radian PI
        {
            get
            {
                return (Radian)Math.PI;
            }
        }
        #endregion

        public Radian(float value)
        {
            InnerValue = value;
        }

        #region Type Casting
        public static explicit operator Radian(float f)
        {
            return new Radian(f);
        }
        public static explicit operator float(Radian degree)
        {
            return degree.InnerValue;
        }
        #endregion

        #region Operator overloading
        public static bool operator ==(Radian a, Radian b)
        {
            return a.InnerValue == b.InnerValue;
        }
        public static bool operator !=(Radian a, Radian b)
        {
            return a.InnerValue != b.InnerValue;
        }
        public static Radian operator +(Radian a, Radian b)
        {
            return new Radian(a.InnerValue + b.InnerValue);
        }
        public static Radian operator -(Radian a, Radian b)
        {
            return new Radian(a.InnerValue - b.InnerValue);
        }
        public static Radian operator /(Radian a, float f)
        {
            return new Radian(a.InnerValue / f);
        }
        public static Radian operator *(Radian a, float f)
        {
            return new Radian(a.InnerValue * f);
        }
        public static Radian operator *(float f, Radian a)
        {
            return new Radian(a.InnerValue * f);
        }
        public static Radian operator -(Radian a)
        {
            return new Radian(-a.InnerValue);
        }
        #endregion
        #region override
        public override Int32 GetHashCode()
        {
            return InnerValue.GetHashCode();
        }
        public override Boolean Equals(Object obj)
        {
            return base.Equals(obj);
        }
        #endregion

        public Vector2 ToUnitVector()
        {
            return new Vector2((float)Math.Cos(InnerValue), (float)Math.Sin(InnerValue));
        }
    }
}
