﻿using System;

namespace Blackbox
{
    [Serializable]
    public struct Second
    {
        private float InnerValue;

        public static Second Zero
        {
            get
            {
                return (Second)0f;
            }
        }

        private Second(float value)
        {
            this.InnerValue = value;
        }

        #region Type Casting
        public static explicit operator Second(float time)
        {
            return new Second(time);
        }
        public static implicit operator float(Second time)
        {
            return time.InnerValue;
        }
        #endregion

        #region Operator overloading
        public static Second operator +(Second lhs, Second rhs)
        {
            return (Second)(lhs.InnerValue + rhs.InnerValue);
        }
        public static Second operator -(Second lhs, Second rhs)
        {
            return (Second)(lhs.InnerValue - rhs.InnerValue);
        }
        public static Second operator *(Second lhs, float rhs)
        {
            return (Second)(lhs.InnerValue * rhs);
        }
        public static Second operator *(float lhs, Second rhs)
        {
            return (Second)(rhs.InnerValue * lhs);
        }
        public static Second operator /(Second lhs, float rhs)
        {
            return (Second)(lhs.InnerValue / rhs);
        }
        public static Second operator -(Second lhs)
        {
            return new Second(-lhs.InnerValue);
        }
        #endregion
    
        public override string ToString()
        {
            return String.Format("[Second: {0}]", InnerValue);
        }
    }
}
